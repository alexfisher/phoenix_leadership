<?php
/**
 * @file
 * phoenix_leadership.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function phoenix_leadership_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_leadership_text|compro_component|leadership|default';
  $field_group->group_name = 'group_leadership_text';
  $field_group->entity_type = 'compro_component';
  $field_group->bundle = 'leadership';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Text',
    'weight' => '1',
    'children' => array(
      0 => 'field_leadership_summary',
      1 => 'field_leadership_title',
      2 => 'field_leadership_name',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Text',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-leadership-text field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_leadership_text|compro_component|leadership|default'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Text');

  return $field_groups;
}
