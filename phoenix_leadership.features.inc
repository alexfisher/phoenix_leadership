<?php
/**
 * @file
 * phoenix_leadership.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function phoenix_leadership_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function phoenix_leadership_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_eck_bundle_info().
 */
function phoenix_leadership_eck_bundle_info() {
  $items = array(
    'compro_component_leadership' => array(
      'machine_name' => 'compro_component_leadership',
      'entity_type' => 'compro_component',
      'name' => 'leadership',
      'label' => 'Leadership',
      'config' => array(
        'managed_properties' => array(
          'title' => 'title',
          'language' => 0,
        ),
      ),
    ),
  );
  return $items;
}
